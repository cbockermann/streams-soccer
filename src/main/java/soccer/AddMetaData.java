/**
 * 
 */
package soccer;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;

import net.minidev.json.JSONObject;
import net.minidev.json.parser.JSONParser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;
import stream.data.DataFactory;
import stream.io.SourceURL;
import stream.util.parser.TimeFormat;

/**
 * @author chris
 * 
 */
public class AddMetaData extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(AddMetaData.class);
	String key = "id";
	SourceURL source;
	final Map<String, Data> metaData = new HashMap<String, Data>();

	public AddMetaData() {
		try {
			source = new SourceURL("classpath:/metadata.json");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * @return the key
	 */
	public String getKey() {
		return key;
	}

	/**
	 * @param key
	 *            the key to set
	 */
	public void setKey(String key) {
		this.key = key;
	}

	/**
	 * @return the source
	 */
	public SourceURL getSource() {
		return source;
	}

	/**
	 * @param source
	 *            the source to set
	 */
	public void setSource(SourceURL source) {
		this.source = source;
	}

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {
		super.init(ctx);

		final JSONParser p = new JSONParser(JSONParser.MODE_PERMISSIVE);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				source.openStream()));
		String line = reader.readLine();
		while (line != null) {

			Data meta = DataFactory.create();
			JSONObject obj = (JSONObject) p.parse(line);
			for (String key : obj.keySet()) {
				meta.put(key, (Serializable) obj.get(key));
			}
			log.info("Adding {}", meta);

			Serializable k = meta.get(key);
			if (k != null) {
				log.debug("Adding meta-data for key '{}'", k);
				metaData.put(k.toString(), meta);
			} else {
				log.debug("Meta data does not contain key '{}': {}", k, meta);
			}

			line = reader.readLine();
		}

		reader.close();
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		if (input == null)
			return input;

		Serializable id = input.get(key);
		if (id == null) {
			return input;
		}

		Double pico = new Double("" + input.get("timestamp"));
		Long time = (pico.longValue() - 10753295594424116L) / (1000000000L);
		// input.put("timestamp", time);

		Data meta = metaData.get(id.toString());
		if (meta != null) {
			// log.debug("Adding meta-data {}", meta);
			input.putAll(meta);
		}

		TimeFormat tf = new TimeFormat();
		String cur = tf.format(time);
		// log.info("game-time: {}", cur);
		input.put("gameTime", cur);
		input.put("@time", time);
		return input;
	}
}
