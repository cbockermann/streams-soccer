/**
 * 
 */
package soccer;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class AddPosition implements Processor {

	static Logger log = LoggerFactory.getLogger(AddPosition.class);
	final Map<String, PositionData> posData = new HashMap<String, PositionData>();

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		final String pid = input.get("team") + ":" + input.get("player");
		// log.info("pos[{}]", pid);
		Double x = new Double(input.get("x") + "");
		Double y = new Double(input.get("y") + "");
		Double z = new Double(input.get("z") + "");

		final Position p = new Position(x, y, z);

		if (!posData.containsKey(pid)) {
			posData.put(pid, new PositionData());
		}

		Position pos = posData.get(pid).add(p);
		input.put("pos", pos);
		input.put("pos.x", pos.x);
		input.put("pos.y", pos.y);
		input.put("pos.z", pos.z);
		return input;
	}

	static class PositionData {

		final Position[] pos = new Position[2];
		int count = 0;

		public Position add(Position p) {
			pos[count % pos.length] = p;
			count++;
			return getPosition();
		}

		public Position getPosition() {

			if (count < 2) {
				return pos[0];
			}

			return new Position((pos[0].x + pos[1].x) / 2,
					(pos[0].y + pos[1].y) / 2, (pos[0].z + pos[1].z) / 2);
		}
	}
}
