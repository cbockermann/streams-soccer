/**
 * 
 */
package soccer;

import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.RenderingHints;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

import javax.swing.JPanel;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * This class provides a panel for drawing a soccer field and several position
 * objects, which have been provided by the update method.
 * 
 * @author Christian Bockermann &lt;christian.bockermann@udo.edu&gt;
 * 
 */
public class Field extends JPanel {

	/** The unique class ID */
	private static final long serialVersionUID = -7149041781268662313L;
	static Logger log = LoggerFactory.getLogger(Field.class);

	final static Color GREEN = new Color(60, 115, 35);
	final double HALF_WIDTH = 52477.0d;
	final double HALF_HEIGHT = 33941.0d;

	String time = null;
	final int border = 40;
	final Map<String, Position> positions = new ConcurrentHashMap<String, Position>();

	public void updateTime(String str) {
		time = str;
	}

	/**
	 * @see javax.swing.JComponent#print(java.awt.Graphics)
	 */
	@Override
	public void paint(Graphics g) {
		super.paint(g);

		int width = this.getWidth();
		int height = this.getHeight();

		((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);

		// fill the panel with a green background
		//
		g.setColor(GREEN);
		g.fillRect(0, 0, width, height);

		// draw the current game-time (if it has been set)
		//
		if (time != null) {
			Font f = g.getFont().deriveFont(10.0f);
			g.setFont(f);
			g.setColor(Color.WHITE);
			g.drawString(time, 4, 12);
		}

		// draw a white border (should be close to field boundaries)
		//
		g.setColor(Color.WHITE);

		int mx = width / 2;
		int my = height / 2;
		int radius = 30;

		g.drawRect(border / 2, border / 2, width - border, height - border);
		g.drawLine(border / 2, height / 2, width - border / 2, height / 2);
		g.drawOval(mx - radius, my - radius, 2 * radius, 2 * radius);

		width -= border;
		height -= border;

		// plot all position objects (players, ball, referee) that have
		// been added via the "update" method (see below)
		//
		for (String key : positions.keySet()) {

			Position pos = positions.get(key);

			// compute the coordinates (x|y) relative to the panel size
			//
			Double x = (pos.x / HALF_WIDTH);
			x = border / 2 + (width * x);

			Double y = (pos.y + HALF_HEIGHT) / (2 * HALF_HEIGHT);
			y = border / 2 + (height * y);

			// player keys are either "TEAM:ID", where TEAM is 'A' or 'B'
			// or 'none' (referee) or 'Ball'
			//
			if (key.indexOf("A") >= 0) {
				g.setColor(Color.BLUE);
			} else {
				g.setColor(Color.RED);
			}

			// the referee has a key of "none:0"
			//
			if (key.indexOf("0") >= 0) {
				g.setColor(Color.BLACK);
			}

			if (key.indexOf("Ball") >= 0) {
				//
				// draw the ball in white, with a slightly smaller radius
				//
				g.setColor(Color.WHITE);
				g.fillOval(x.intValue() - 2, y.intValue() - 2, 4, 4);
			} else
				g.fillOval(x.intValue() - 3, y.intValue() - 3, 6, 6);
		}
	}

	public void update(String key, Position p) {

		// update the position with the given key
		// and repaint the graphics
		//
		positions.put(key, p);

		repaint();
		validate();
	}
}