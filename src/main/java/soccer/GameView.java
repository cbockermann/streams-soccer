/**
 * 
 */
package soccer;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.AbstractProcessor;
import stream.Data;
import stream.ProcessContext;

/**
 * @author chris
 * 
 */
public class GameView extends AbstractProcessor {

	static Logger log = LoggerFactory.getLogger(GameView.class);
	final JFrame frame = new JFrame();
	final Field field = new Field();

	/**
	 * @see stream.AbstractProcessor#init(stream.ProcessContext)
	 */
	@Override
	public void init(ProcessContext ctx) throws Exception {

		super.init(ctx);
		frame.getContentPane().setLayout(new BorderLayout());
		frame.getContentPane().add(field, BorderLayout.CENTER);

		frame.setSize(576, 768);
		frame.setVisible(true);
	}

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		String id = input.get("team") + ":" + input.get("player");
		Position pos = (Position) input.get("pos");
		// if (id.equals("B:8")) {
		// log.info("{} ({}) = " + pos, id, input.get("id"));
		// log.info("   {}", input);
		// log.info("   {} {}", input.get("pos.x"), input.get("pos.y"));
		// }
		field.updateTime(input.get("gameTime") + "");
		field.update(id, pos);
		return input;
	}
}