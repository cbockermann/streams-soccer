/**
 * 
 */
package soccer;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class PicoToMillis implements Processor {

	String key = "timestamp";

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		if (input == null || !input.containsKey(key))
			return input;

		Double d = new Double("" + input.get(key));
		input.put(key, d / (1000.0 * 1000.0d));
		return input;
	}
}
