/**
 * 
 */
package soccer;

import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import stream.Data;
import stream.Processor;

/**
 * @author chris
 * 
 */
public class PlayerDistance implements Processor {

	int i = 0;
	static Logger log = LoggerFactory.getLogger(PlayerDistance.class);
	final Map<String, Double> teamDistance = new HashMap<String, Double>();
	final Map<String, Double> distances = new HashMap<String, Double>();
	final Map<String, Position> lastPosition = new HashMap<String, Position>();

	/**
	 * @see stream.Processor#process(stream.Data)
	 */
	public Data process(Data input) {

		if (!input.containsKey("pos") || !input.containsKey("type"))
			return null;

		final Position pos = (Position) input.get("pos");

		String team = input.get("team") + "";
		String pid = team + ":" + input.get("player");
		String type = input.get("type").toString();

		if (type.indexOf("arm") < 0) {

			Position last = lastPosition.get(pid);
			if (last != null) {
				double diff = last.distance(pos);
				add(pid, diff);
				addTeam(team, diff);
			}
			lastPosition.put(pid, pos);
		}

		if (i % 100 != 0)
			return null;

		for (String k : distances.keySet()) {
			double d = distances.get(k);
			input.put("distance:" + k, d);
		}

		for (String t : teamDistance.keySet()) {
			input.put("distance:Team:" + t, teamDistance.get(t));
		}

		return input;
	}

	protected double add(String id, double dist) {
		Double cur = distances.get(id);
		if (cur != null) {
			cur += dist;
			distances.put(id, cur);
			return cur;
		} else {
			distances.put(id, dist);
			return dist;
		}
	}

	protected double addTeam(String id, double dist) {
		Double cur = teamDistance.get(id);
		if (cur != null) {
			cur += dist;
			teamDistance.put(id, cur);
			return cur;
		} else {
			teamDistance.put(id, dist);
			return dist;
		}
	}
}
