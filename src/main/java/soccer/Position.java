/**
 * 
 */
package soccer;

import java.io.Serializable;

/**
 * @author chris
 * 
 */
public class Position implements Serializable {

	/** The unique class ID */
	private static final long serialVersionUID = 8158292538962533229L;

	public final Double x;
	public final Double y;
	public final Double z;

	public Position(final int x, final int y, final int z) {
		this.x = Double.valueOf(x);
		this.y = Double.valueOf(y);
		this.z = Double.valueOf(z);
	}

	public Position(final double x, final double y, final double z) {
		this.x = Double.valueOf(x);
		this.y = Double.valueOf(y);
		this.z = Double.valueOf(z);
	}

	public double distance(Position p) {

		final double dx = x - p.x;
		final double dy = y - p.y;

		return Math.sqrt((dx * dx) + (dy * dy));
	}

	public String toString() {
		return "{ x=" + x + ", y=" + y + ", z=" + z + " }";
	}
}
